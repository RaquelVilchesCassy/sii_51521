// Mundo.h: interface for the CMundo class.
//
//////////////////////////////////////////////////////////////////////
/////////CABECERA MODIFICADA POR CRISTOBAL LABRADO IGLESIAS//////////
//////////////PARA LA PRACTICA 1 DE LABORATORIO DE SII///////////////
/////////////////////////////////////////////////////////////////////
///////////////////Grupo A (Martes 15:15-17:15)//////////////////////
/////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"
#include "DatosMemCompartida.h" //Aquí se declara Raqueta y Esfera
#include <pthread.h>
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class SMundo  
{
public:
	void Init();
	SMundo();
	virtual ~SMundo();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	
	void RecibeComandosJugador();

	Esfera esfera;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;

	int puntos1;
	int puntos2;
	int fd_logger;
	int fd_cliente;
	int fd_servidor;
	pthread_t thd;
	int fin_cliente;

	DatosMemCompartida archivo_bot;
	DatosMemCompartida *p_archivo_bot;
	char *proyeccion;
};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
