// Mundo.cpp: implementation of the SMundo class
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoServidor.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/file.h>
#include <sys/mman.h>
#include <fcntl.h>

#define MAX 200
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
void* hilo_comandos(void* d)
{
      SMundo* p=(SMundo*) d;

	//Apertura del FIFO Cliente -> Servidor (lectura)
	p->fd_cliente=open("/tmp/FIFO_cliente",O_RDONLY);
		if (p->fd_cliente==-1){
		perror("Error al abrir FIFO_cliente\n");
		exit(1);}

       p->RecibeComandosJugador();
      pthread_exit (0);
}
SMundo::SMundo()
{
	Init();
}

SMundo::~SMundo()
{
//Informe de cierre al logger y cierre del FIFO_logger
	char buffer[MAX];
	sprintf(buffer,"%s","Fin del juego");
	write(fd_logger,buffer,strlen(buffer)+1);
	close (fd_logger);

//informa de cierre del servidor al cliente y cierre de FIFO_servidor en caso de que el cliente no se haya cerrado antes
	if (fin_cliente==0){
		sprintf(buffer,"%s","Fin del juego");
		write(fd_servidor,buffer,strlen(buffer)+1);
	}
	close (fd_servidor);
	close (fd_cliente);
//eliminación del thread
	pthread_join(thd, NULL);
	if (fin_cliente)
	pthread_join(thd, NULL);
}

void SMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
 	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);

 	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

 	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

 	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

 	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

 	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );

  	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

 	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

 	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

 	glEnable( GL_DEPTH_TEST );
}

 void SMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

 	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();

	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)  
  
	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

  	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

 	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

  	//Al final, cambiar el buffer
	glutSwapBuffers();
}

 void SMundo::OnTimer(int value)
{	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
 	esfera.Mueve(0.025f);
	int i;
	char cad[MAX];

	for(i=0;i<paredes.size();i++)
	{

 		paredes[i].Rebota(esfera);

 		paredes[i].Rebota(jugador1);

 		paredes[i].Rebota(jugador2);

 	}

 

 	jugador1.Rebota(esfera);

 	jugador2.Rebota(esfera);

 	if(fondo_izq.Rebota(esfera))

 	{

 	char buffer[MAX];

 		esfera.centro.x=0;

 		esfera.centro.y=rand()/(float)RAND_MAX;

 		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;

 		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;

 		puntos2++;

 		sprintf(buffer, "Jugador 2: %d", puntos2);

 		write (fd_logger, buffer, strlen(buffer)+1);

 	}

 

 	if(fondo_dcho.Rebota(esfera))

 	{

 	char buffer[MAX];

 		esfera.centro.x=0;

 		esfera.centro.y=rand()/(float)RAND_MAX;

 		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;

 		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;

 		puntos1++;

 		sprintf(buffer, "Jugador 1: %d", puntos1);

 		write (fd_logger, buffer, strlen(buffer)+1);

 	}


//Envío de datos por la tubería hacia el cliente (si el cliente sigue abierto)
if (fin_cliente==0){
	sprintf(cad,"%f %f %f %f %f %f %f %f %f %f %d %d", esfera.centro.x,esfera.centro.y, jugador1.x1,jugador1.y1,jugador1.x2,jugador1.y2,jugador2.x1,jugador2.y1,jugador2.x2,jugador2.y2, puntos1, puntos2);

 	write(fd_servidor, cad, strlen(cad)+1);

}
}


void SMundo::RecibeComandosJugador()
{
if(fin_cliente==0){
     while (1) {
            usleep(10);
           char cad[MAX];
           read(fd_cliente, cad, sizeof(cad));
       if (cad[0]=='F') {
        fin_cliente=1; 
		break;}
      else{
            unsigned char key;    
          sscanf(cad,"%c",&key);
            if(key=='s')jugador1.velocidad.y=-4;
            if(key=='w')jugador1.velocidad.y=4;
            if(key=='l')jugador2.velocidad.y=-4;
            if(key=='o')jugador2.velocidad.y=4;
          }
    }
    exit(0);
   }

 }


 void SMundo::Init()

 {
	fin_cliente=0;

 	Plano p;

 //pared inferior

 	p.x1=-7;p.y1=-5;

 	p.x2=7;p.y2=-5;

 	paredes.push_back(p);

 

 //superior

 	p.x1=-7;p.y1=5;

 	p.x2=7;p.y2=5;

 	paredes.push_back(p);

 

 	fondo_izq.r=0;

 	fondo_izq.x1=-7;fondo_izq.y1=-5;

 	fondo_izq.x2=-7;fondo_izq.y2=5;

 

 	fondo_dcho.r=0;

 	fondo_dcho.x1=7;fondo_dcho.y1=-5;

 	fondo_dcho.x2=7;fondo_dcho.y2=5;

 

 //a la izq

 	jugador1.g=0;

 	jugador1.x1=-6;jugador1.y1=-1;

 	jugador1.x2=-6;jugador1.y2=1;

 

 //a la dcha

 	jugador2.g=0;

 	jugador2.x1=6;jugador2.y1=-1;

 	jugador2.x2=6;jugador2.y2=1;

 	

 //Apertura del FIFO_logger (escritura)

 	fd_logger=open("/tmp/FIFO_logger",O_WRONLY);

 	if(fd_logger==-1){

 	perror("open");

 	}

 //Apertura del FIFO Servidor->Cliente (escritura)

 	fd_servidor=open("/tmp/FIFO_servidor",O_WRONLY);

 		if (fd_servidor==-1){

 		perror("Error al abrir FIFO_servidor\n");

 		exit(1);}
		
//Creación del thread
	pthread_create(&thd, NULL, hilo_comandos, this);
	

 }
