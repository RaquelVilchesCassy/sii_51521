#include <sys/types.h> 
#include <sys/stat.h>
#include <stdio.h>
#include <ctype.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>

#define MAX 200

int main(){

int fd;
char ch;
char buffer[MAX];
int aux_error;
//Eliminación inicial del FIFO para evitar problemas (en caso de no haber sido borrado antes
	unlink("/tmp/FIFO_logger");

//Creación del FIFO
	unlink("/tmp/FIFO_logger");
	aux_error=mkfifo("/tmp/FIFO_logger", 0666);

	if (aux_error==-1){
	printf ("El fifo ya existe\n");
	unlink("/tmp/FIFO_logger");
	exit(1);}

 

//Apertura del FIFO

		fd=open("/tmp/FIFO_logger", O_RDONLY);
	if (fd==-1){
	perror(":open()");
	exit(1);
	}

//En caso de una apertura correcta: Muestra la puntuación por terminal
	printf("\nInicio del juego:\nJugador 1:0 - Jugador 2:0\n\n");

while(1){

	aux_error=read(fd,buffer, sizeof(buffer));
	if (aux_error==-1){
		unlink("/tmp/FIFO_logger");
		printf("Error de lectura del FIFO\n");
		exit(1);
	}
	else if(buffer[0]=='F'){
		printf("%s\n", buffer);
		break;
		}
	else
		printf("%s\n", buffer);
}

//Cierre y borrado de la tubería al cerrarse el proceso escritor

close(fd);
unlink("/tmp/FIFO_logger");
exit(1);
return 0;
}
