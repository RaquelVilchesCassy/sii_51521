// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoCliente.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/file.h>
#include <sys/mman.h>
#include <fcntl.h>

#define MAX 200
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{
char cad[MAX];

if (fin_servidor==0){ 
//Se informa al servidor de la muerte del cliente
	sprintf(cad,"%s","Fin del juego");
	write(fd_cliente, cad, strlen(cad)+1);
}
//Cierre y borrado de la tubería Servidor->CLiente
	close(fd_servidor);
	unlink("/tmp/FIFO_servidor");

//Cierre y borrado de la tubería CLiente->Servidor
	close(fd_cliente);
	unlink("/tmp/FIFO_servidor");

//Se informa el bot del fin del juego y se desproyecta
	p_archivo_bot->fin_del_juego=1; 
	munmap(proyeccion,sizeof(archivo_bot));

	unlink("/tmp/datosBot");
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
 	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
	int i;
	char cad[MAX];
	int aux_error;


	//Recepción de datos de la tubería Servidor-> Cliente

	aux_error=read(fd_servidor,cad, sizeof(cad));
	if (aux_error==-1){
		unlink("/tmp/FIFO_servidor");
		printf("Error de lectura del FIFO_servidor\n");
		exit(1);}
	
	else if (cad[0]=='F') {
	fin_servidor=1; //En caso de que el servidor termine, cambiamos la variable fin_juego
	exit(1);}	
	
	if (fin_servidor==0){
	sscanf(cad,"%f %f %f %f %f %f %f %f %f %f %d %d", &esfera.centro.x,&esfera.centro.y, &jugador1.x1,&jugador1.y1,&jugador1.x2,&jugador1.y2, &jugador2.x1,&jugador2.y1,&jugador2.x2,&jugador2.y2, &puntos1, &puntos2);

 //Modificamos los datos que necesita el bot
	p_archivo_bot->esfera=esfera;
	p_archivo_bot->raqueta1=jugador1;

	if (p_archivo_bot->accion==1)
	OnKeyboardDown('w',0,0);
	else if (p_archivo_bot->accion==-1)
	OnKeyboardDown('s',0,0);
	else if (p_archivo_bot->accion==0); 
	}
}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
char cad[MAX];
//Informamos a MundoServidor de las teclas pulsadas
	sprintf(cad,"%c",key);
	write(fd_cliente, cad, strlen(cad)+1);
}

void CMundo::Init()

{
	int aux_error;
	fin_servidor=0;
	Plano p;
//Creacion del fichero de memoria proyectada para el bot
 	int file=open("/tmp/datosBot",O_RDWR|O_CREAT|O_TRUNC, 0777);
 		if (file==-1){
			printf ("\n Error de apertura del fichero donde se quiere proyectar \n");
 		}
 		
//Asignamos el tamaño del fichero escribiendo en él.
	write(file,&archivo_bot,sizeof(archivo_bot));
	 
	proyeccion=(char*)mmap(NULL,sizeof(archivo_bot),PROT_WRITE|PROT_READ,MAP_SHARED,file,0); 

	close(file); //Cerramos el fichero
	p_archivo_bot=(DatosMemCompartida*)proyeccion; 
	p_archivo_bot->accion=0;
	p_archivo_bot->fin_del_juego=0; 
	
//esfera
	esfera.centro.x=0;
	esfera.centro.y=0;

//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
	
//Eliminación del fifo para evitar errores
	unlink("/tmp/FIFO_servidor");

//Creación del fifo Servidor-> Cliente
	aux_error=mkfifo("/tmp/FIFO_servidor",0666);
		if(aux_error==-1){
		perror("\n Ya existe el FIFO_servidor \n");
		}
//Apertura del fifo Servidor -> Cliente
	fd_servidor=open("/tmp/FIFO_servidor",O_RDONLY);
		if(fd_servidor==-1){
		perror("open");
		}
//Creación del fifo Cliente -> Servidor
	unlink("/tmp/FIFO_cliente");
	aux_error=mkfifo("/tmp/FIFO_cliente",0666);
		if(aux_error==-1){
		perror("\n Ya existe el FIFO_cliente \n");
		}
//Apertura del fifo Cliente -> Servidor
	fd_cliente=open("/tmp/FIFO_cliente",O_WRONLY);
		if(fd_cliente==-1){
		perror("open");
		}

	}
