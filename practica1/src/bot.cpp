 #include <iostream>
 #include <sys/types.h>
 #include <sys/stat.h>
 #include <sys/mman.h>
 #include <fcntl.h>
 #include <unistd.h>
 #include "DatosMemCompartida.h"
 //#include "Esfera.h"
 //#include "Raqueta.h"

 int main()
 {
 
     int file;
     DatosMemCompartida* p_archivo_bot;
     char* proyeccion;
 	int aux_err;

    if( (file=open("/tmp/datosBot",O_RDWR))<0){
    		 perror ("Error al abrir el fichero bot");
    		 return -1;}

     //Proyección del fichero
     if ((proyeccion=(char*)mmap(NULL,sizeof(p_archivo_bot),PROT_WRITE|PROT_READ,MAP_SHARED,file,0))==MAP_FAILED){
     perror("No puede abrirse el fichero"); 
     return -1;}
     close(file);

     p_archivo_bot=(DatosMemCompartida*)proyeccion;

     //Movimiento automatico de la raqueta
     while(1)
     {
         float centro_Raqueta;
         centro_Raqueta=((p_archivo_bot->raqueta1.y1+p_archivo_bot->raqueta1.y2)/2);
         
         if(centro_Raqueta<p_archivo_bot->esfera.centro.y)
            p_archivo_bot->accion=1;

         else if(centro_Raqueta>p_archivo_bot->esfera.centro.y)
             p_archivo_bot->accion=-1;

         else
             p_archivo_bot->accion=0;
         usleep(25000);  //

        if (p_archivo_bot->fin_del_juego) break;
     }


     munmap(proyeccion,sizeof(p_archivo_bot));
     return 1;

     }
